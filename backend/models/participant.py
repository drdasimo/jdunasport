from . import db
from .event import Event
from .user import User

class Participant(db.Model):
    participant_id = db.Column(db.Integer, primary_key=True)
    event_id = db.Column(db.Integer, db.ForeignKey('event.event_id'), nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('user.user_id'), nullable=False)
    sub = db.Column(db.Boolean, nullable=False)
    event = db.relationship('Event', backref=db.backref('participants', lazy=True))
    user = db.relationship('User', backref=db.backref('participations', lazy=True))
