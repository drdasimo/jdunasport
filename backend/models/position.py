from . import db
from .event import Event

class Position(db.Model):
    position_id = db.Column(db.Integer, primary_key=True)
    event_id = db.Column(db.Integer, db.ForeignKey('event.event_id'), nullable=False)
    name = db.Column(db.String(40), nullable=False)
    count = db.Column(db.Integer, nullable=False)
    max_count = db.Column(db.Integer, nullable=False)
    event = db.relationship('Event', backref=db.backref('positions', lazy=True))
