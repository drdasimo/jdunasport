from . import db
from .user import User

class Event(db.Model):
    event_id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(40), nullable=False)
    info = db.Column(db.Text)
    datetime = db.Column(db.DateTime, nullable=False)
    place = db.Column(db.String(40), nullable=False)
    created_by = db.Column(db.Integer, db.ForeignKey('user.user_id'), nullable=False)
    created_at = db.Column(db.DateTime, nullable=False)
    creator = db.relationship('User', backref=db.backref('events_created', lazy=True))
