from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from models import db
from models.user import User
from models.event import Event
from models.position import Position
from models.participant import Participant

# Initialize Flask app
app = Flask(__name__)

# Configure SQLAlchemy for MySQL
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:@localhost/database_name'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

# Bind SQLAlchemy to the Flask app
db.init_app(app)

# Create database tables
with app.app_context():
    db.create_all()
